class Node:

  def __init__(self, conn):
    self.conn = conn
    self.data = {}
    self['uid'] = 0
    self['name'] = ''
    self['changed'] = 0
    
  def __getitem__(self, key):
    return self.data[key]

  def __setitem__(self, key, item):
    self.data[key] = item
  
  # Define Drupal-related helper methods
  def save(self):
    if (self['uid'] == 0):
      self['uid'] = conn.user['uid']
    if (self['name'] == ''):
      self['name'] = conn.user['name']
    if (self['changed'] == 0):
      import time;
      self['changed'] = int(time.time())
    result = self.conn.server.node.save(self.conn.key, self.conn.sessid, self.data)
    self.data = {}
    self.data = result
  
  def load(self, fields = []):
    nid = int(self['nid'])
    if (nid > 0):
      result = self.conn.server.node.load(self.conn.key, self.conn.sessid, nid, fields)
      self.data = {}
      self.data = result
  
  def delete(self):
    nid = int(self['nid'])
    if (nid > 0):
      result = self.conn.server.node.delete(self.conn.key, self.conn.sessid, nid)
      self.data = {}
      
      