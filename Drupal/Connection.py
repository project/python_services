class XMLRPC:
  def __init__(self, url, key, username, password):
    import xmlrpclib;
    self.key = key
    self.server = xmlrpclib.Server(url)
    self.connection = self.server.system.connect(key)
    self.session = self.server.user.login(key, self.connection['sessid'], username, password)
    self.sessid = self.session['sessid']
    self.user = self.session['user']