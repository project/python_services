import unittest
import pprint, sys, time

class TestCreateLoadSaveNode(unittest.TestCase):

  def setUp(self):
    from config import config
    self.config = config
    self.pp = pprint.PrettyPrinter(indent=2)
  
  def tearDown(self):
    # Log user out
    pass

  def testConnectToDrupal(self):
    import Drupal.Connection as Connection
    self.pp.pprint(Connection)
    conn = Connection.XMLRPC(self.config['url'], self.config['key'], self.config['username'], self.config['password'])
    methods = conn.server.system.listMethods()
    self.assertEqual(methods.count('system.listMethods'), 1, 'system.listMethods called successfully')

  # 
  # Test creating, loading, updating and deleting nodes
  # 
  def testCreateLoadUpdateDeleteNode(self):
    import Drupal.Connection as Connection
    import Drupal.Node as Node
    conn = Connection.XMLRPC(self.config['url'], self.config['key'], self.config['username'], self.config['password'])
    # Create
    node = Node.Node(conn)
    node['type'] = 'story'
    node['title'] = 'A Python Test'
    node['body'] = 'lorem ipsum'
    node['uid'] = conn.user['uid']
    node['name'] = conn.user['name']
    node['changed'] = int(time.time())
    node.save()
    self.assert_(node['nid'], 'Created node')
    # Load
    newNode = Node.Node(conn)
    newNode['nid'] = node['nid']
    newNode.load()
    self.assertEqual(newNode['nid'], node['nid'], 'Loaded node')
    # Update
    node['title'] = 'Modified title'
    node.save()
    self.assertEqual(node['title'], 'Modified title', 'Updated node')
    # Delete
    node.delete()
    self.assert_(node.data == {}, 'Deleted node')

if __name__ == '__main__':
  unittest.main()
